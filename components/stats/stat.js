import * as dayjs from 'dayjs'
import relativeTime from 'dayjs/plugin/relativeTime'

dayjs.extend(relativeTime)

export default function Stat({ title, date, count }) {
  return (
    <div className="border dark:border-gray-700 border-gray-300 p-4 rounded-md dark:text-white">
      <div className="flex justify-between">
        <span className="dark:text-gray-100 font-bold">{title}</span>
        <span className="dark:text-gray-400 text-sm">{date && dayjs(date).fromNow()}</span>
      </div>
      <div className="text-2xl truncate mt-1 font-bold">{count}</div>
    </div>
  )
}
