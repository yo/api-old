import Stat from './stat'
import useSWR from 'swr'
import axios from 'axios'
import Skeleton, { SkeletonTheme } from 'react-loading-skeleton'

const fetcher = (url) => axios.get(url);

export default function Home({ dark }) {
  const loader = (
    <SkeletonTheme
      color={dark ? "#374151" : "#E8E8E8"}
      highlightColor={dark ? "#4B5563" : "#CACACA"}
    >
      <Skeleton />
    </SkeletonTheme>
  );
  const { data: todoCount } = useSWR(`/api/todo`, fetcher);
  const { data: latestMood } = useSWR(`/api/mood`, fetcher);
  const { data: spotify } = useSWR(`/api/spotify`, fetcher);
  const { data: wakatime } = useSWR(`/api/wakatime`, fetcher);
  return (
    <div className="pt-10">
      <div className="dark:text-white mb-5">
        <span className="text-3xl font-bold">Stats </span>
        <span className="text-sm dark:text-gray-400 font-bold">(Realtime)</span>
      </div>
      <div className="gap-4 grid grid-cols-1 my-2 sm:grid-cols-2 w-full">
        <Stat
          date={latestMood && latestMood['data']['created_at']}
          title="Latest mood"
          count={latestMood ? latestMood['data']['mood'] : loader}
        />
        <Stat
          date={false}
          title={spotify ? spotify['data']['playing'] ? 'Listening to' : 'Last listened to' : '-'}
          count={spotify ? `${spotify['data']['artist']} - ${spotify['data']['name']}` : loader}
        />
        <Stat
          date={false}
          title="Pending Todos"
          count={todoCount ? todoCount['data']['open'] : loader}
        />
        <Stat
          date={false}
          title="Hours Spent Coding (7 days)"
          count={wakatime ? `${wakatime['data']['total']}` : loader}
        />
      </div>
    </div>
  )
}
