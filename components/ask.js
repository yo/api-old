import React, { useState } from 'react'
import Head from 'next/head'
import Link from 'next/link'
import toast, { Toaster } from 'react-hot-toast'
import axios from 'axios'

export default function Ask() {
  const [question, setQuestion] = useState('');
  const handleSubmit = async (event) => {
    event.preventDefault();
    toast.loading('Submiting in progress!');
    await axios.post(`/api/ask`,
      { 'title': question },
      { headers: { "Access-Control-Allow-Origin": "*" }}
    )
    .then((response) => {
      toast.dismiss();
      toast.success('Successfully asked!');
      setQuestion('');
    })
    .catch(() => {
      toast.dismiss();
      toast.error('Something went wrong!');
    });
  }

  return (
    <form onSubmit={handleSubmit}>
      <textarea
        className="w-full focus:outline-none px-4 py-2 dark:text-white rounded shadow-sm focus:border-blue-500 border border-gray-200 dark:border-gray-800 bg-white dark:bg-gray-1000 block"
        placeholder="Ask me anything..."
        value={question}
        onChange={e => setQuestion(e.target.value)}
      ></textarea>
      {question &&
        <button
          className="bg-green-500 dark:bg-green-500 dark:text-white font-bold mt-4 px-3 py-1.5 rounded"
          type="submit"
        >
          Ask
        </button>
      }
      <Toaster />
      <div className="w-full border-t border-gray-200 dark:border-gray-800 mt-4"></div>
    </form>
  )
}
