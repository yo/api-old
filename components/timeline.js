import timeline from '../data/timeline';
import { CheckCircle } from 'react-feather';
import ReactMarkdown from 'react-markdown';

export default function Timeline() {
  return (
    <div className="pt-10">
      <div className="dark:text-white mb-5">
        <span className="text-3xl font-bold">Timeline</span>
      </div>
      <div className="w-full">
        {timeline.years.map((timeline, index) => {
          return (
            <div className="my-6 border-b-2 dark:border-gray-800 border-gray-300" key={index}>
              <div className="dark:text-white text-2xl font-bold">{timeline.year}</div>
              <div className="ml-3">
                {timeline.events.map((event, index) => {
                  return (
                    <div className="my-8" key={index}>
                      <div className="dark:text-white font-bold flex items-center">
                        <CheckCircle className="dark:text-green-500 text-green-600" size={18} />
                        <span className="ml-2">{event.title}</span>
                      </div>
                      <div className="text-gray-400 mt-2 ml-6">
                        <ReactMarkdown>{event.description}</ReactMarkdown>
                      </div>
                    </div>
                  )
                })}
              </div>
            </div>
          )
        })}
      </div>
    </div>
  )
}
