import projects from '../data/projects';
import Link from 'next/link';
import ReactMarkdown from 'react-markdown';

export default function Projects({ limit }) {
  return (
    <div className="pt-10">
      <div className="dark:text-white mb-5">
        <span className="text-3xl font-bold">Projects</span>
      </div>
      <div className="w-full">
        {projects.slice(0, limit).map((project, index) => {
          return (
            <div className="my-6" key={index}>
              <div className="flex items-center border dark:border-gray-700 border-gray-300 p-4 rounded-md dark:text-white">
                <img
                  className="rounded-lg mr-3 w-14 h-14"
                  src={project.logo}
                  alt={project.title}
                />
                <div>
                  <div className="text-2xl dark:text-gray-200 font-bold">
                    <Link href={project.link}>{project.title}</Link>
                  </div>
                  <div className="dark:text-gray-400 mt-2">
                    <ReactMarkdown>{project.description}</ReactMarkdown>
                  </div>
                </div>
              </div>
            </div>
          )
        })}
        {projects.length > limit ? (
          <div>
            <Link href="/projects">
              <a className="dark:bg-gray-800 bg-gray-200 flex font-bold justify-around py-1.5 rounded dark:text-white">
                Show more
              </a>
            </Link>
          </div>
        ) : ''}
      </div>
    </div>
  )
}
