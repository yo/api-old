import Head from 'next/head'
import Layout from '../components/layout'
import Hero from '../components/hero'
import { getSortedPostsData } from '../lib/posts'
import Link from 'next/link'
import * as dayjs from 'dayjs'

export default function Blog({ allPostsData }) {
  return (
    <Layout blog>
      <Head>
        <title>WIP</title>
      </Head>
      <section>
        <Hero
          title="Blog"
          description="I love writing tech articles here are my list of blogs."
        />
      </section>
      <section>
        <ul>
          {allPostsData.map(({ id, date, title, description }) => (
            <li className="my-6" key={id}>
              <Link href={`/posts/${id}`}>
                <a className="text-lg text-blue-400 font-bold">{title}</a>
              </Link>
              <div className="dark:text-white mt-1">{description}</div>
              <div className="text-sm text-gray-400 mt-2">
                Created at {dayjs(date).format('MMM D, YYYY')}
              </div>
            </li>
          ))}
        </ul>
      </section>
    </Layout>
  )
}

export async function getStaticProps() {
  const allPostsData = getSortedPostsData()
  return {
    props: {
      allPostsData
    }
  }
}
