import React, { useState, useEffect } from 'react';
import Head from 'next/head'
import Layout from '../components/layout'
import Hero from '../components/hero'
import Ask from '../components/ask'
import { getSortedPostsData } from '../lib/posts'
import Link from 'next/link'
import * as dayjs from 'dayjs'
import useSWR from 'swr'
import axios from 'axios'
import Skeleton, { SkeletonTheme } from 'react-loading-skeleton'

const fetcher = (url) => axios.get(url);

export default function AMA({ allPostsData }) {
  const [dark, setDark] = useState(true);
  const { data: issues } = useSWR(`https://gitlab.com/api/v4/projects/24562472/issues?state=closed&labels=Answered&per_page=100`, fetcher);

  useEffect(() => {
    setDark(window.matchMedia('(prefers-color-scheme: dark)').matches);
  });

  return (
    <Layout blog>
      <Head>
        <title>AMA - Yoginth</title>
      </Head>
      <section>
        <Hero
          title="Ask Me Anything"
          description="Just for fun! Questions will be visible after I’ve answered."
        />
      </section>
      <section>
        <Ask />
      </section>
      <section>
        {
          issues ? (
            <ul>
            {issues.data.map(function(issue, index) {
              return (
                <li className="my-6" key={issue.id}>
                  <Link href={`https://gitlab.com/yo/ama/-/issues/${issue.iid}`}>
                    <a className="text-lg text-blue-400 font-bold">{issue.title}</a>
                  </Link>
                  <div className="text-sm text-gray-400 mt-2">
                    Created at {dayjs(issue.updated_at).format('MMM D, YYYY')}
                  </div>
                </li>
              );
            })}
            </ul>
          ) : (
            [...Array(5)].map((e, i) => (
              <SkeletonTheme
                color={dark ? "#374151" : "#E8E8E8"}
                highlightColor={dark ? "#4B5563" : "#CACACA"}
              >
                <Skeleton height={20} width={500} style={{ marginTop: 30 }} />
                <Skeleton height={15} width={200} style={{ marginTop: 10 }} />
              </SkeletonTheme>
            ))
          )
        }
      </section>
    </Layout>
  )
}

export async function getStaticProps() {
  const allPostsData = getSortedPostsData()
  return {
    props: {
      allPostsData
    }
  }
}
