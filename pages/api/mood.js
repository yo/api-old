import axios from "axios";

export default async (req, res) => {
  try {
    const GITLAB_TOKEN = process.env.GITLAB_TOKEN;

    const response = await axios.get(
      `https://gitlab.com/api/v4/projects/23590604/issues?access_token=${GITLAB_TOKEN}`
    );
    const data = response.data[0];

    res.statusCode = 200;
    return res.json({
      mood: data.title,
      created_at: data.created_at,
    });
  } catch (err) {
    console.log(err);
    res.statusCode = 500;
    return res.json({ message: "💩" });
  }
};
