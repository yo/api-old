import axios from "axios";

export default async (req, res) => {
  try {
    const {
      query: { range },
    } = req;
    var time_range;

    switch (parseInt(range)) {
      case 7:
        time_range = "last_7_days";
        break;
      case 30:
        time_range = "last_30_days";
        break;
      case 180:
        time_range = "last_6_months";
        break;
      case 365:
        time_range = "last_year";
        break;
      default:
        time_range = "last_7_days";
        break;
    }

    const response = await axios.get(
      `https://wakatime.com/api/v1/users/current/stats/${time_range}?api_key=${process.env.WAKATIME_API_KEY}`
    );
    const data = response.data["data"];

    res.statusCode = 200;
    return res.json({
      total: data.human_readable_total,
      updated_at: data.modified_at,
    });
  } catch (err) {
    console.log(err);
    res.statusCode = 500;
    return res.json({ message: "💩" });
  }
};
