import axios from "axios";

export default async (req, res) => {
  try {
    const options = {
      headers: {'PRIVATE-TOKEN': process.env.GITLAB_BOT_TOKEN}
    };
    const response = await axios.post(
      `https://gitlab.com/api/v4/projects/24562472/issues`,
      {
        'title': req.body.title
      }, options)
      .then((response) => {
        res.statusCode = 200;
        return res.json({ status: true });
      }, (error) => {
        res.statusCode = 500;
        return res.json({ status: false });
      });
  } catch (err) {
    console.log(err);
    res.statusCode = 500;
    return res.json({ status: false });
  }
};
