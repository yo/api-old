import axios from "axios";

export default async (req, res) => {
  try {
    const get_track = await axios.get(
      `http://ws.audioscrobbler.com/2.0/?method=user.getrecenttracks&user=${process.env.LASTFM_USERNAME}&api_key=${process.env.LASTFM_API_KEY}&format=json`
    );
    const get_scrobbles = await axios.get(
      `http://ws.audioscrobbler.com/2.0/?method=user.getinfo&user=${process.env.LASTFM_USERNAME}&api_key=${process.env.LASTFM_API_KEY}&format=json`
    );

    const track = get_track.data['recenttracks']['track'];
    const scrobbles = get_scrobbles.data['user']['playcount'];
    const playing = track[0]["@attr"] !== undefined;

    res.statusCode = 200;
    return res.json({
      name: track[0]["name"],
      artist: track[0]["artist"]["#text"],
      album: track[0]["album"]["#text"],
      url: track[0]["url"],
      image: track[0]["image"][3]["#text"],
      playing,
      scrobbles,
    });
  } catch (err) {
    res.statusCode = 500;
    return res.json({ message: "💩" });
  }
};
