import axios from "axios";

export default async (req, res) => {
  try {
    const response = await axios.get(
      `https://gitlab.com/api/v4/todos?access_token=${process.env.GITLAB_TOKEN}&per_page=10000`
    );
    const data = response.data;

    res.statusCode = 200;
    return res.json({
      open: data.length,
    });
  } catch (err) {
    console.log(err);
    res.statusCode = 500;
    return res.json({ message: "💩" });
  }
};
