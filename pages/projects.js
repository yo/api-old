import Head from 'next/head';
import Layout from '../components/layout';
import Hero from '../components/hero';
import Projects from '../components/projects';

export default function Project() {
  return (
    <Layout projects>
      <Head>
        <title>Yogi</title>
      </Head>
      <section>
        <Hero
          title="Projects"
          description="I'm Creator of Taskord, GitLab Hero and Dev.to Moderator living in India."
        />
      </section>
      <Projects />
    </Layout>
  )
}
