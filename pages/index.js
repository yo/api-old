import React, { useState, useEffect } from 'react';
import Head from 'next/head';
import Layout from '../components/layout';
import Hero from '../components/hero';
import Stats from '../components/stats/stats';
import Timeline from '../components/timeline';
import Projects from '../components/projects';

export default function Home() {
  const [dark, setDark] = useState(true);
  useEffect(() => {
    setDark(window.matchMedia('(prefers-color-scheme: dark)').matches);
  });

  return (
    <Layout home>
      <Head>
        <title>Yogi</title>
      </Head>
      <section>
        <Hero
          title="Hey, I’m Yogi"
          description="I'm Creator of Taskord, GitLab Hero and Dev.to Moderator living in India."
        />
      </section>
      <Stats dark={dark} />
      <Timeline />
      <Projects limit={3} />
    </Layout>
  )
}
