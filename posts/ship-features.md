---
title: "How to ship small features rapidly to production 🚀"
description: "How to ship rapidly to production"
date: "2021-01-09"
---

Hey folks 👋

I'm the creator of [Taskord](https://taskord.com), I deploy Taskord more than 100 times daily.

In this article, I'm gonna explain how to **ship rapidly to production** without any error and let us see what the heck is **Staff Ship** and feature flags!

## 🌱 Planning

Let us consider we are building a brand new **Explore page** and `/explore` is going to be the URL. Plan about

- How is the **UI** going to look
- Do we have all **UI components** available
- **Data model** and what are the **tables** we need to create for a **database**
- Any **blocking** Issues

Don't think a lot and waste the time, we can add/improve it in the next Iteration.

## 🌵 First Pull/Merge Request

After the proper planning just go ahead and create the first page just saying `Hello, World!` and point it to `/explore` in your routes file.

After that commit and push to GitHub/GitLab and create your first pull request.

![Screenshot 2021-01-09 173436](https://dev-to-uploads.s3.amazonaws.com/i/9fqpueazuqgot6ei7h0v.png)

> This step is optional if you are a solo developer, you can push all the way straight to `main`/`master` branch (but it is not suggested).

## 👨‍💻 Development

After creating the pull request start concentrating on developing the app. If you have any dependency on the backend team don't wait for them until they complete.

Instead, just mock the JSON Response and start developing the work, make sure to give the JSON Spec to the backend team to develop the API in such a way you want. If not we need to rework again based on their API response.

**_Communication and time are crucial._**

## 🧪 Testing

After completing the development wait for the CI to become green for that pull request.

![Screenshot 2021-01-09 175331](https://dev-to-uploads.s3.amazonaws.com/i/9849dlcgaaxfz4nfq56g.png)

Once all the tests have been passed merge the branch to `main`/`production`.

Deploy the feature to your staging instance, so you can deploy to production servers without fear. Test everything well, create a small checklist and check everything is done and working fine in staging instance before moving to production.

### What is feature flags/toggle?

Feature flags are simply a toggle button which has the ability to turn features of your application on/off easily based on condition.

## 🛡 Staff Release

![Screenshot 2021-01-09 182645](https://dev-to-uploads.s3.amazonaws.com/i/bg08uqagn3onq47e60bv.png)

After testing well, deploy `main` branch to production servers where the new feature exists.

Post in your internal slack that _"Hey, we just shipped the explore page to our production servers 🐿, make sure to tun on staff ship. Feedbacks welcomed 😊"_

Then sit back and relax for some time till the feedback arrives. If it has any bugs fix it ASAP.

### By the way what is staff ship 🤔

Staff ship is a feature flag which is available only for site admins possibly mods, where admin can tun on the flag and check whether the feature is working or not.

When staff ship is enabled admins can see this special admin/performance bar above the navbar which is heavily inspired by GitHub.com

**Taskord's admin bar**

![Screenshot 2021-01-09 181035](https://dev-to-uploads.s3.amazonaws.com/i/ef0ncehewxgsqjcst3t8.png)

**GitHub's admin bar** (GH Enterprise)

![3](https://dev-to-uploads.s3.amazonaws.com/i/gai7ydvykwzz7vw0rolu.png)

Here is the sample video how staff ship works in [Taskord](https://taskord.com), the staff ship is enabled when pressing backtick or `pb` shortcut key in the keyboard which is available only for admins.

{% youtube OhXQc8I-GpU %}

## 👥 Beta Release

![Screenshot 2021-01-09 182727](https://dev-to-uploads.s3.amazonaws.com/i/7azsaqxkfo449069yit9.png)

Release the `/explore` page for selected users, usually, you can do by shipping to

- Users based on age
- 50% of platform users
- Users with beta flag enabled, etc.

## 💬 Feedback

Feedbacks are critical, you will get a lot if the userbase is huge, make sure to evaluate quickly and fix the bugs in upcoming **Iterations**.

### Automatic Error Tracking

Error Tracking is also a part of feedback, this need be done automatically. We use [Sentry](https://sentry.io) to track errors and patch the fix quickly.

**This is how Sentry looks like**

![Screenshot 2021-01-09 182053](https://dev-to-uploads.s3.amazonaws.com/i/dzx820fc868jqor6rmdc.png)

## 🌎 General Availability

![Screenshot 2021-01-09 182805](https://dev-to-uploads.s3.amazonaws.com/i/eyz156cw9qo9kyrlzqzj.png)

If all settled and good to go, just go ahead and remove all the feature flags for `/explore` page. Make sure to keep an eye on Sentry for a few days.

## ⚠ Be careful

- Prepare to revert the PR anytime if there is an issue in production ↩
- Commit every change 😅

Hope this article helped you 😊

**Happy Shipping 🚀**
